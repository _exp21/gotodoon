const colors = require("tailwindcss/colors");

module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    extends: {
      colors: {
        primary: "#5c6ac4",
        "dm-background": {
          // The background colors if darkmode
          light: "",
          DEFAULT: colors.gray["600"],
          dark: "",
        },
        background: {
          // The background colors
          light: colors.gray["200"],
          DEFAULT: "#E5E7EB",
          dark: "",
        },
        "dm-foreground": {
          // The foreground colors if dark mode
        },
        foreground: {
          // The foreground colors
        },
        primary: {
          // The primary accent color (should work on on light and darkmode)
          light: "",
          DEFAULT: "",
          dark: "",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
