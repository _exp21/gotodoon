
// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": preferDefault(require("/home/ewt/gatsby------/gotodoon/.cache/dev-404-page.js")),
  "component---src-pages-404-js": preferDefault(require("/home/ewt/gatsby------/gotodoon/src/pages/404.js")),
  "component---src-pages-app-js": preferDefault(require("/home/ewt/gatsby------/gotodoon/src/pages/app.js")),
  "component---src-pages-dev-playground-js": preferDefault(require("/home/ewt/gatsby------/gotodoon/src/pages/dev/playground.js")),
  "component---src-pages-index-js": preferDefault(require("/home/ewt/gatsby------/gotodoon/src/pages/index.js"))
}

