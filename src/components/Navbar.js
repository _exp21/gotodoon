import React from "react";
import ThemeToggle from "./themeToggle.js";
import SearchBox from "./searchBox";
import { Link } from "gatsby";
export default function Navbar() {
  return (
    <>
      {/* Navbar */}
      <nav className="absolute hidden md:block  bg-gray-400 dark:bg-gray-700 top-0 left-0 w-full z-10 bg-transparent md:flex-row md:flex-nowrap md:justify-start flex items-center p-4">
        <div className="w-full mx-autp items-center flex justify-between md:flex-nowrap flex-wrap md:px-10 px-4">
          {/* Search bar */}
          <SearchBox />
          <ul className="flex-col md:flex-row list-none items-center hidden md:flex">
            {/* Theme Toggler */}
            <li>
              <ThemeToggle />
            </li>
            <li className="ml-3">
              <Link to="/app/settings">
                <i className="fas fa-cog text-gray-800 dark:text-white text-2xl"></i>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
      {/* End Navbar */}
    </>
  );
}
