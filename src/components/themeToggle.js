import React, { useEffect, useState } from "react";
import { ThemeToggler } from "gatsby-plugin-dark-mode";

export default function ThemeToggle() {
  // themetoggle state
  const [themeToggle, setthemeToggle] = useState(false);

  //change theme
  const changeTheme = (e, toggleTheme) => {
    toggleTheme(e.target.checked ? "dark" : "light");
    setthemeToggle(!themeToggle);
  };

  //default theme
  useEffect(() => {
    if (localStorage.theme === "dark") {
      setthemeToggle(true);
    } else {
      setthemeToggle(false);
    }
  }, []);
  return (
    <ThemeToggler>
      {({ theme, toggleTheme }) => (
        <label>
          <input
            className="hidden absolute"
            type="checkbox"
            onChange={(e) => changeTheme(e, toggleTheme)}
            checked={theme === "dark"}
          />{" "}
          {themeToggle ? (
            <div className="light:hidden dark:block flex text-white">
              <i className="fas fa-moon text-white-400 text-2xl mr-2"></i>
            </div>
          ) : (
            <div className="light:block dark:hidden flex">
              <i className="fas fa-sun text-yellow-400 text-2xl mr-2"></i>
            </div>
          )}
        </label>
      )}
    </ThemeToggler>
  );
}
