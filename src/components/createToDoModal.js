import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

const schema = yup.object().shape({
  title: yup.string().required("Required."),
  tags: yup.string().required("Required."),
  description: yup.string().required("Required."),
});

export default function CreateToDoModal({ setShowModal, handleFormSubmit }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  }); // initialize the hook
  const formSubmit = (data) => {
    handleFormSubmit(data);
  };
  return (
    <>
      <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-full my-6 mx-auto max-w-screen-xl z-50">
          {/*content*/}
          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white dark:bg-gray-500 outline-none focus:outline-none">
            {/*header*/}
            <div className="flex items-start justify-between p-5 rounded-t">
              <h3 className="text-3xl font-semibold dark:text-white">
                Create New ToDo
              </h3>
              <button
                className="p-1 ml-auto bg-transparent border-0 text-black opacity-1 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                onClick={() => setShowModal(false)}
              >
                <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                  &times;
                </span>
              </button>
            </div>
            {/*body*/}
            <div className="relative p-6 flex-auto">
              {/* form */}

              <div className="grid grid-cols-3 gap-4">
                <div className="col-span-2 flex">
                  {/* Inputs */}

                  <div className="w-3 mr-4">
                    <i className="fas fa-thumbtack text-gray-700 py-3"></i>
                  </div>
                  <div className="w-full">
                    <form onSubmit={handleSubmit(formSubmit)} noValidate>
                      <div className="mb-3">
                        <input
                          className="bg-gray-300 w-full py-2 px-3 md:py-4 md:px-6 text-gray-700  leading-tight focus:outline-none dark:bg-gray-600 dark:text-white rounded"
                          name="title"
                          type="text"
                          placeholder="To Do"
                          {...register("title")}
                        />
                        <p className="text-red-500">{errors?.title?.message}</p>
                      </div>
                      <div className="mb-3">
                        <input
                          className="bg-gray-300 w-full py-2 px-3 md:py-4 md:px-6 text-gray-700  leading-tight focus:outline-none dark:bg-gray-600 dark:text-white rounded"
                          id="tags"
                          type="text"
                          name="tags"
                          placeholder="tags"
                          {...register("tags")}
                        />
                        <p className="text-red-500">{errors?.tags?.message}</p>
                      </div>
                      <div className="mb-3">
                        <textarea
                          className="form-textarea block w-full bg-gray-300 text-gray-700 dark:bg-gray-600 dark:text-white  px-6 py-3 rounded"
                          rows="6"
                          placeholder="Enter some long form content."
                          name="description"
                          {...register("description")}
                        ></textarea>
                        <p className="text-red-500">
                          {errors?.description?.message}
                        </p>
                      </div>
                      {/* Inputs */}
                      <div className="flex items-center justify-end py-4">
                        <button
                          className="bg-blue-500 dark:bg-gray-600 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                          type="submit"
                        >
                          Save Changes
                        </button>
                        <button
                          className="text-white bg-gray-400 font-bold uppercase px-6 py-3 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear rounded shadow hover:shadow-lg  transition-all duration-150"
                          type="button"
                          onClick={() => setShowModal(false)}
                        >
                          Cancel
                        </button>
                      </div>
                    </form>
                  </div>
                </div>

                <div>
                  <label className="block">
                    <select className="form-select bg-gray-300 text-gray-700 dark:bg-gray-600 dark:text-white block w-full mb-3 px-6 py-4 h-13 rounded">
                      <option>Due</option>
                      <option>Option 1</option>
                      <option>Option 2</option>
                    </select>
                  </label>
                  <label className="block">
                    <select className="form-select bg-gray-300 text-gray-700 dark:bg-gray-600 dark:text-white block w-full mb-3 px-6 py-4 h-13 rounded">
                      <option>Inbox</option>
                      <option>Option 1</option>
                      <option>Option 2</option>
                    </select>
                  </label>
                  <label className="block">
                    <select className="form-select bg-gray-300 text-gray-700 dark:bg-gray-600 dark:text-white block w-full mb-3 px-6 py-4 h-13 rounded">
                      <option>Standalone</option>
                      <option>Option 1</option>
                      <option>Option 2</option>
                    </select>
                  </label>
                </div>
              </div>

              {/* form */}
            </div>
          </div>
        </div>
        <div
          className="opacity-25 fixed inset-0 z-40 bg-black"
          onClick={() => setShowModal(false)}
        ></div>
      </div>
    </>
  );
}
