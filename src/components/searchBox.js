import React from "react";

export default function SearchBox() {
  return (
    <>
      <div className="bg-white dark:bg-gray-400 flex items-center rounded-full shadow-xl ml-auto md:mx-auto w-8/12 md:w-6/12 h-10 md:h-14">
        <input
          className="ml-1 rounded-l-full w-full py-2 px-3 md:py-4 md:px-6 text-gray-700 leading-tight focus:outline-none dark:bg-gray-600 dark:text-white"
          id="search"
          type="text"
          placeholder="Search"
        />
        <div className="py-4 px-1">
          <button className="bg-gray-500 dark:bg-gray-800 text-white rounded-full py-2 px-6 hover:bg-blue-400 focus:outline-none h-8 md:h-12 flex items-center justify-center">
            <i className="fas fa-search"></i>
          </button>
        </div>
      </div>
    </>
  );
}
