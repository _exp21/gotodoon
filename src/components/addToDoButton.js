import React from "react";

export default function AddToDoButton({ setShowModal }) {
  return (
    <>
      <button
        className="hidden md:block text-gray-400 dark:text-white fixed right-4 bottom-4 text-6xl active:bg-pink-600 outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
        type="button"
        onClick={() => setShowModal(true)}
      >
        <i className="far fa-plus-square"></i>
      </button>
    </>
  );
}
