import React, { useState, useEffect } from "react";
import { DataStore, Predicates, SortDirection } from "aws-amplify";
import { Todo, StateName } from "../models/index";
import Navbar from "./Navbar";
import AddToDoButton from "./addToDoButton";
import CreateToDoModal from "./createToDoModal";
import Sidebar from "./sidebar";

const Dashboard = () => {
  //model state
  const [showModal, setShowModal] = useState(false);
  const [showTodoData, setShowTodoData] = useState(false);

  //handle submit
  const handleFormSubmit = async (data) => {
    console.log(data);
    // handle data inputs to submit the data
    let newTodo = new Todo({
      title: data.title,
      state: StateName.NEW,
    });
    const savedTodo = await DataStore.save(newTodo);
    getTodoList();
    // close model
    setShowModal(false);
  };

  const [TodoList, setTodoList] = useState([]);

  const getTodoList = async () => {
    try {
      const todoList = await DataStore.query(Todo, Predicates.ALL, {
        sort: (s) => s.title(SortDirection.DESCENDING),
      });
      setTodoList(todoList);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getTodoList();
  }, []);

  return (
    <>
      <Sidebar />
      <div
        className={`relative md:ml-64  bg-gray-200 dark:bg-gray-600 min-h-screen`}
      >
        <Navbar />

        <div className="relative md:pt-32 pb-10 pt-12">
          <div className="px-4 md:px-10 mx-auto w-full ">
            <p className="dark:text-white">list of Todos:</p>

            <div className="w-full md:w-3/5  p-8 my-6">
              <div className="shadow-md">
                {/* <div className="tab w-full overflow-hidden border-t mb-3">
                  <div className="px-2 flex items-center relative bg-gray-300 dark:bg-gray-800 dark:text-white rounded">
                    <input
                      className="opacity-0"
                      id="tab-multi-one"
                      type="checkbox"
                      name="tabs"
                    />
                    <label
                      className="block p-5 leading-normal cursor-pointer w-full"
                      htmlFor="tab-multi-one"
                    >
                      <i className="fas fa-star"></i> Label One
                    </label>
                  </div>
                  {false ? (
                    <div className="bg-white tab-content overflow-hidden border-l-2 bg-gray-100 border-indigo-500 leading-normal">
                      gsggbvfbgbvg f hrgr fd ghrngrghe wegffhd verghrg rgcxvnerg
                    </div>
                  ) : null}
                </div> */}

                {TodoList &&
                  TodoList.map((todo) => (
                    <div
                      className="tab w-full overflow-hidden border-t mb-3"
                      key={todo.id}
                    >
                      <div className="px-2 flex items-center py-4 relative bg-gray-300 dark:bg-gray-800 dark:text-white rounded">
                        <p
                          onClick={() => {
                            setShowTodoData(todo.id);
                          }}
                        >
                          <i className="fas fa-star"></i> {todo.title}
                        </p>
                      </div>
                      {showTodoData === todo.id ? (
                        <div className="bg-white tab-content overflow-hidden border-l-2 bg-gray-100 border-indigo-500 leading-normal">
                          <p>
                            this will be a long description of four to five
                            lines
                          </p>
                          <p>todo status</p>
                          <p>tag1 , tag2, tag3 [in left side ]</p>
                          <p>created at : time [ in right bottom side ] </p>
                        </div>
                      ) : null}
                    </div>
                  ))}
              </div>
            </div>

            <AddToDoButton setShowModal={setShowModal} />

            {/* create todo modal */}
            {showModal ? (
              <CreateToDoModal
                showModal={showModal}
                setShowModal={setShowModal}
                handleFormSubmit={handleFormSubmit}
              />
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
};

export default Dashboard;
