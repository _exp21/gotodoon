import React from "react";
import Navbar from "./Navbar";

import Sidebar from "./sidebar";

const Waiting = () => {
  return (
    <>
      <Sidebar />
      <div className="relative md:ml-64 bg-gray-200 dark:bg-gray-600 min-h-screen">
        <Navbar />

        <div className="relative md:pt-32 pb-10 pt-12">
          <div className="px-4 md:px-10 mx-auto w-full ">
            <p className="dark:text-white">we are waiting here.......</p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Waiting;
