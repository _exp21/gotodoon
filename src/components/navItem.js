import React from "react";
import { Link } from "gatsby";

export default function NavItem({ navigationURL, icon, title }) {
  return (
    <Link
      className="text-blueGray-700 hover:text-blueGray-500 text-xs uppercase py-3 font-bold block dark:text-white"
      to={navigationURL}
    >
      <i className={icon}></i> {title}
    </Link>
  );
}
