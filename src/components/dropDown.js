import React from "react";
import { createPopper } from "@popperjs/core";

const Dropdown = () => {
  // dropdown props
  const [dropdownPopoverShow, setDropdownPopoverShow] = React.useState(false);
  const btnDropdownRef = React.createRef();
  const popoverDropdownRef = React.createRef();
  const openDropdownPopover = () => {
    createPopper(btnDropdownRef.current, popoverDropdownRef.current, {
      placement: "bottom-start",
    });
    setDropdownPopoverShow(true);
  };
  const closeDropdownPopover = () => {
    setDropdownPopoverShow(false);
  };
  // bg colors

  return (
    <>
      <div className="flex flex-wrap">
        <div className="w-full sm:w-6/12 md:w-4/12 px-4">
          <div className="relative inline-flex align-middle w-full">
            <a
              className="text-blueGray-500 block dark:text-white"
              href="#pablo"
              ref={btnDropdownRef}
              onClick={(e) => {
                e.preventDefault();
                dropdownPopoverShow
                  ? closeDropdownPopover()
                  : openDropdownPopover();
              }}
            >
              Context
            </a>
            <div
              ref={popoverDropdownRef}
              className={
                (dropdownPopoverShow ? "block " : "hidden ") +
                "bg-blueGray-700 " +
                "text-base z-50 float-left py-2 list-none text-left rounded shadow-lg mt-1"
              }
              style={{ minWidth: "12rem" }}
            >
              <a
                href="#"
                className={
                  "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent dark:text-white"
                }
                onClick={(e) => e.preventDefault()}
              >
                @home
              </a>
              <a
                href="#"
                className={
                  "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent dark:text-white"
                }
                onClick={(e) => e.preventDefault()}
              >
                @office
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dropdown;
