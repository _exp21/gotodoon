import React from "react";
import { Link } from "gatsby";
// import "./sidebar.css";

import NavItem from "./navItem.js";
import Dropdown from "./dropDown.js";

export default function Sidebar() {
  const [collapseShow, setCollapseShow] = React.useState(" navslide");

  //navitems
  const navitems = [
    {
      id: 1,
      title: "Inbox",
      navigationURL: "/app",
      icon: "fas fa-envelope text-blueGray-400 mr-2 text-sm",
    },
    {
      id: 2,
      title: "Doing",
      navigationURL: "/app/doing",
      icon: "fas fa-play text-blueGray-400 mr-2 text-sm",
    },
    {
      id: 3,
      title: "Waiting",
      navigationURL: "/app/waiting",
      icon: "fas fa-pause text-blueGray-400 mr-2 text-sm",
    },
    {
      id: 4,
      title: "Scheduled",
      navigationURL: "/app/scheduled",
      icon: "fas fa-clipboard-list text-blueGray-400 mr-2 text-sm",
    },
    {
      id: 5,
      title: "Maybe / Someday",
      navigationURL: "/app",
      icon: "fas fa-circle-notch text-blueGray-400 mr-2 text-sm",
    },
  ];

  return (
    <>
      <nav className="dark:bg-gray-800 bg-white md:left-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-nowrap md:overflow-hidden shadow-xl bg-white flex flex-wrap items-center justify-between relative md:w-64 z-10 py-4 px-6">
        <div className="md:flex-col md:items-stretch md:min-h-full md:flex-nowrap px-0 flex flex-wrap items-center justify-between w-full mx-auto">
          {/* Toggler */}
          <button
            className="cursor-pointer text-black dark:text-white opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
            type="button"
            onClick={() => setCollapseShow(" ")}
          >
            <i className="fas fa-bars"></i>
          </button>
          {/* Brand */}
          <Link
            className="md:block text-left md:pb-2 text-blueGray-600 mr-0 inline-block whitespace-nowrap text-sm uppercase font-bold p-4 px-0 dark:text-white"
            to="/"
          >
            GoToDo
          </Link>
          {/* User */}
          <ul className="md:hidden items-center flex flex-wrap list-none">
            <li className="inline-block relative"></li>
            <li className="inline-block relative">{/* <UserDropdown /> */}</li>
          </ul>
          {/* Collapse */}
          <div
            className={
              "md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none shadow absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 transition-transform rounded bg-white dark:bg-gray-800 md:min-h-full min-h-screen px-2 md:p-0 w-4/5 md:w-full" +
              collapseShow
            }
          >
            {/* Collapse header */}
            <div className="md:min-w-full md:hidden block pb-4 mb-4 border-b border-solid border-blueGray-200">
              <div className="flex flex-wrap">
                <div className="w-6/12">
                  <Link
                    className="md:block dark:text-white text-left md:pb-2 text-blueGray-600 mr-0 inline-block whitespace-nowrap text-sm uppercase font-bold p-4 px-0"
                    to="/"
                  >
                    GoToDo
                  </Link>
                </div>
                <div className="w-6/12 flex justify-end">
                  <button
                    type="button"
                    className="cursor-pointer text-black dark:text-white opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
                    onClick={() => setCollapseShow(" navslide")}
                  >
                    <i className="fas fa-times"></i>
                  </button>
                </div>
              </div>
            </div>
            {/* Form */}
            <form className="mt-6 mb-4 md:hidden">
              <div className="mb-3 pt-0">
                <input
                  type="text"
                  placeholder="Search"
                  className="border-0 px-3 py-2 h-12 border border-solid  border-blueGray-500 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-base leading-snug shadow-none outline-none focus:outline-none w-full font-normal"
                />
              </div>
            </form>
            {/* Navigation */}
            <ul className="md:flex-col md:min-w-full flex flex-col list-none">
              {/* nav items */}
              {navitems &&
                navitems.map((item) => (
                  <li className="items-center" key={item.id}>
                    <NavItem
                      navigationURL={item.navigationURL}
                      icon={item.icon}
                      title={item.title}
                    />
                  </li>
                ))}
              {/* nav items */}
            </ul>
            {/* Divider */}
            <hr className="my-4 md:min-w-full" />
            {/* Dropdowns */}
            <Dropdown />
          </div>
        </div>
      </nav>
    </>
  );
}
