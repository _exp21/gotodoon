// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';

const StateName = {
  "NEW": "new",
  "REFINED": "refined",
  "DELEGATED": "delegated",
  "SCHEDULED": "scheduled",
  "POSTPONED": "postponed",
  "DONE": "done"
};

const { Todo, TodoTag, Tag, Project, Context } = initSchema(schema);

export {
  Todo,
  TodoTag,
  Tag,
  Project,
  Context,
  StateName
};