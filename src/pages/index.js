import React from "react"
import SEO from '../components/seo'
import Layout from '../components/layout'

const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Home"/>
      <div className="text-center">
        <p className="text-4xl font-semibold">
          Get things done.
        </p>
        <p className="text-xl text-gray-600">
          gotodo helps you with keeping track, and getting stuff of your mind.
        </p>
        <p>
          Do with your mind what it is good at, keep everything else in a secure place. Any Device, Any Time. <br/>
          gotodo is a machine learning driven to do app, that helps you structure and organize your tasks.
        </p>
      </div>
    </Layout>
  )
}

export default IndexPage
