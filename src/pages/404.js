import React from "react"
import {Link} from "gatsby"
import SEO from "../components/seo"

const NotFoundPage = () => {
  return (
    <React.Fragment>
      <SEO title="Not found"/>
      <Link to="/">Go home</Link>.
    </React.Fragment>
  )
}

export default NotFoundPage
