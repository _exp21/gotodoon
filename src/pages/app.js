import React from "react";
import { Router } from "@reach/router";
import { withAuthenticator } from "@aws-amplify/ui-react";
import Amplify from "aws-amplify";
import Dashboard from "../components/dashboard";
import Settings from "../components/settings";
import Doing from "../components/doing";
import Waiting from "../components/waiting";
import Scheduled from "../components/scheduled";

Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: "eu-central-1",
    userPoolId: "eu-central-1_Jq5JVZutG",
    userPoolWebClientId: "63ac66vtgskfb7sfj4nerhucce",
  },
  aws_appsync_graphqlEndpoint:
    "https://mec4ngxit5d6znxn2m7chjexcq.appsync-api.eu-central-1.amazonaws.com/graphql\n",
  aws_appsync_region: "eu-central-1",
  aws_appsync_authenticationType: "AMAZON_COGNITO_USER_POOLS",
});

const App = () => {
  return (
    <Router>
      <Dashboard path="/app" />
      <Settings path="/app/settings" />
      <Doing path="/app/doing" />
      <Waiting path="/app/waiting" />
      <Scheduled path="/app/scheduled" />
    </Router>
  );
};

export default withAuthenticator(App);
