import React from 'react'
import Amplify, {DataStore} from "aws-amplify";
import {Context, Project, StateName, Tag, Todo, TodoTag} from "../../models";
import {withAuthenticator} from "@aws-amplify/ui-react";

Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: 'eu-central-1',
    userPoolId: 'eu-central-1_Jq5JVZutG',
    userPoolWebClientId: '63ac66vtgskfb7sfj4nerhucce'
  },
  aws_appsync_graphqlEndpoint: "https://mec4ngxit5d6znxn2m7chjexcq.appsync-api.eu-central-1.amazonaws.com/graphql",
  aws_appsync_region: "eu-central-1",
  aws_appsync_authenticationType: "AMAZON_COGNITO_USER_POOLS"
});


const Playground = () => {
  const createNewProject = async () => {
    const newProject = await DataStore.save(new Project({
      name: "My Awesome Project"
    }))

    console.log("New Project:")
    console.log(newProject)
    console.log("-------")
  }

  const updateProjectColor = async () => {
    const project = await DataStore.query(Project, condition => condition.name("eq", "My Awesome Project"))
    const first = project[0];
    const updatedProject = await DataStore.save(Project.copyOf(first, x => {
      x.color = "Green"
    }))

    console.log("Updated Project:")
    console.log(updatedProject)
    console.log("-------")
  }

  const createNewContext = async () => {
    const newContext = await DataStore.save(new Context({
      name: "computer"
    }))

    console.log("New Context")
    console.log(newContext)
    console.log("-------")
  }
  
  const createNewTag = async () => {
    const newTag = await DataStore.save(new Tag({
      name: 'Tag1',
    }))
    
    console.log("New Tag:")
    console.log(newTag)
    console.log("-------")
  }

  const createNewTodo = async () => {
    const newTodo = await DataStore.save(
      new Todo({
        title: "test",
        state: StateName.NEW,
      })
    )

    console.log("New Todo")
    console.log(newTodo)
    console.log("-------")
  }

  const patchTodo = async () => {
    const todo = await DataStore.query(Todo, predicate => predicate.title("eq","test"))
    const first = todo[0]
    const patchedTodo = Todo.copyOf(first, updated => {
      updated.description = "1234 Description"
      updated.title = "new Title"
    })

    console.log("Patched Todo Description for update")
    console.log(patchedTodo)
    console.log("-------")

    const updatedTodo = await DataStore.save(patchedTodo)

    console.log("Updated Todo context")
    console.log(updatedTodo)
    console.log("-------")
  }

  const addTagToTodo = async () => {
    const tags = await DataStore.query(Tag, predicate => predicate.name("eq","Tag1"))
    const first = tags[0]
    const todos = await DataStore.query(Todo, predicate => predicate.title("eq", "test"))
    const firstTodo = todos[0]

    const todoTag = await DataStore.save(new TodoTag({
      tag: first,
      todo: firstTodo
    }))

    console.log("TodoTag link:")
    console.log(todoTag)
  }

  return (
    <div className="text-center">
      <p>
        <button onClick={createNewProject}>Create New Project</button>
      </p>
      <p>
        <button onClick={updateProjectColor}>Update Project Color</button>
      </p>
      <p>
        <button onClick={createNewContext}>Create New Context</button>
      </p>
      <p>
        <button onClick={createNewTodo}>Create New Todo</button>
      </p>
      <p>
        <button onClick={createNewTag}>Create New Tag</button>
      </p>
      <p>
        <button onClick={patchTodo}>Update Todo Description</button>
      </p>
      <p>
        <button onClick={addTagToTodo}>Add Tag To Todo</button>
      </p>
    </div>
  )
}

export default withAuthenticator(Playground)