/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createTodo = /* GraphQL */ `
  mutation CreateTodo(
    $input: CreateTodoInput!
    $condition: ModelTodoConditionInput
  ) {
    createTodo(input: $input, condition: $condition) {
      id
      tags {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      todoProjectId
      project {
        id
        name
        color
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todoContextId
      context {
        id
        name
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      title
      description
      state
      waitingFor
      scheduledOn
      doneOn
      owner
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
    }
  }
`;
export const updateTodo = /* GraphQL */ `
  mutation UpdateTodo(
    $input: UpdateTodoInput!
    $condition: ModelTodoConditionInput
  ) {
    updateTodo(input: $input, condition: $condition) {
      id
      tags {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      todoProjectId
      project {
        id
        name
        color
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todoContextId
      context {
        id
        name
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      title
      description
      state
      waitingFor
      scheduledOn
      doneOn
      owner
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
    }
  }
`;
export const deleteTodo = /* GraphQL */ `
  mutation DeleteTodo(
    $input: DeleteTodoInput!
    $condition: ModelTodoConditionInput
  ) {
    deleteTodo(input: $input, condition: $condition) {
      id
      tags {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      todoProjectId
      project {
        id
        name
        color
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todoContextId
      context {
        id
        name
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      title
      description
      state
      waitingFor
      scheduledOn
      doneOn
      owner
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
    }
  }
`;
export const createTag = /* GraphQL */ `
  mutation CreateTag(
    $input: CreateTagInput!
    $condition: ModelTagConditionInput
  ) {
    createTag(input: $input, condition: $condition) {
      id
      name
      color
      todos {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const updateTag = /* GraphQL */ `
  mutation UpdateTag(
    $input: UpdateTagInput!
    $condition: ModelTagConditionInput
  ) {
    updateTag(input: $input, condition: $condition) {
      id
      name
      color
      todos {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const deleteTag = /* GraphQL */ `
  mutation DeleteTag(
    $input: DeleteTagInput!
    $condition: ModelTagConditionInput
  ) {
    deleteTag(input: $input, condition: $condition) {
      id
      name
      color
      todos {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const createProject = /* GraphQL */ `
  mutation CreateProject(
    $input: CreateProjectInput!
    $condition: ModelProjectConditionInput
  ) {
    createProject(input: $input, condition: $condition) {
      id
      name
      color
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const updateProject = /* GraphQL */ `
  mutation UpdateProject(
    $input: UpdateProjectInput!
    $condition: ModelProjectConditionInput
  ) {
    updateProject(input: $input, condition: $condition) {
      id
      name
      color
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const deleteProject = /* GraphQL */ `
  mutation DeleteProject(
    $input: DeleteProjectInput!
    $condition: ModelProjectConditionInput
  ) {
    deleteProject(input: $input, condition: $condition) {
      id
      name
      color
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const createContext = /* GraphQL */ `
  mutation CreateContext(
    $input: CreateContextInput!
    $condition: ModelContextConditionInput
  ) {
    createContext(input: $input, condition: $condition) {
      id
      name
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const updateContext = /* GraphQL */ `
  mutation UpdateContext(
    $input: UpdateContextInput!
    $condition: ModelContextConditionInput
  ) {
    updateContext(input: $input, condition: $condition) {
      id
      name
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const deleteContext = /* GraphQL */ `
  mutation DeleteContext(
    $input: DeleteContextInput!
    $condition: ModelContextConditionInput
  ) {
    deleteContext(input: $input, condition: $condition) {
      id
      name
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const createTodoTag = /* GraphQL */ `
  mutation CreateTodoTag(
    $input: CreateTodoTagInput!
    $condition: ModelTodoTagConditionInput
  ) {
    createTodoTag(input: $input, condition: $condition) {
      id
      todoId
      tagId
      tag {
        id
        name
        color
        todos {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todo {
        id
        tags {
          nextToken
          startedAt
        }
        todoProjectId
        project {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todoContextId
        context {
          id
          name
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        title
        description
        state
        waitingFor
        scheduledOn
        doneOn
        owner
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateTodoTag = /* GraphQL */ `
  mutation UpdateTodoTag(
    $input: UpdateTodoTagInput!
    $condition: ModelTodoTagConditionInput
  ) {
    updateTodoTag(input: $input, condition: $condition) {
      id
      todoId
      tagId
      tag {
        id
        name
        color
        todos {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todo {
        id
        tags {
          nextToken
          startedAt
        }
        todoProjectId
        project {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todoContextId
        context {
          id
          name
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        title
        description
        state
        waitingFor
        scheduledOn
        doneOn
        owner
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteTodoTag = /* GraphQL */ `
  mutation DeleteTodoTag(
    $input: DeleteTodoTagInput!
    $condition: ModelTodoTagConditionInput
  ) {
    deleteTodoTag(input: $input, condition: $condition) {
      id
      todoId
      tagId
      tag {
        id
        name
        color
        todos {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todo {
        id
        tags {
          nextToken
          startedAt
        }
        todoProjectId
        project {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todoContextId
        context {
          id
          name
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        title
        description
        state
        waitingFor
        scheduledOn
        doneOn
        owner
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
      owner
    }
  }
`;
