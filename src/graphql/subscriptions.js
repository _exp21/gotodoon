/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateTodo = /* GraphQL */ `
  subscription OnCreateTodo($owner: String!) {
    onCreateTodo(owner: $owner) {
      id
      tags {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      todoProjectId
      project {
        id
        name
        color
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todoContextId
      context {
        id
        name
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      title
      description
      state
      waitingFor
      scheduledOn
      doneOn
      owner
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
    }
  }
`;
export const onUpdateTodo = /* GraphQL */ `
  subscription OnUpdateTodo($owner: String!) {
    onUpdateTodo(owner: $owner) {
      id
      tags {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      todoProjectId
      project {
        id
        name
        color
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todoContextId
      context {
        id
        name
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      title
      description
      state
      waitingFor
      scheduledOn
      doneOn
      owner
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
    }
  }
`;
export const onDeleteTodo = /* GraphQL */ `
  subscription OnDeleteTodo($owner: String!) {
    onDeleteTodo(owner: $owner) {
      id
      tags {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      todoProjectId
      project {
        id
        name
        color
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todoContextId
      context {
        id
        name
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      title
      description
      state
      waitingFor
      scheduledOn
      doneOn
      owner
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
    }
  }
`;
export const onCreateTag = /* GraphQL */ `
  subscription OnCreateTag($owner: String!) {
    onCreateTag(owner: $owner) {
      id
      name
      color
      todos {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onUpdateTag = /* GraphQL */ `
  subscription OnUpdateTag($owner: String!) {
    onUpdateTag(owner: $owner) {
      id
      name
      color
      todos {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onDeleteTag = /* GraphQL */ `
  subscription OnDeleteTag($owner: String!) {
    onDeleteTag(owner: $owner) {
      id
      name
      color
      todos {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onCreateProject = /* GraphQL */ `
  subscription OnCreateProject($owner: String!) {
    onCreateProject(owner: $owner) {
      id
      name
      color
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onUpdateProject = /* GraphQL */ `
  subscription OnUpdateProject($owner: String!) {
    onUpdateProject(owner: $owner) {
      id
      name
      color
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onDeleteProject = /* GraphQL */ `
  subscription OnDeleteProject($owner: String!) {
    onDeleteProject(owner: $owner) {
      id
      name
      color
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onCreateContext = /* GraphQL */ `
  subscription OnCreateContext($owner: String!) {
    onCreateContext(owner: $owner) {
      id
      name
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onUpdateContext = /* GraphQL */ `
  subscription OnUpdateContext($owner: String!) {
    onUpdateContext(owner: $owner) {
      id
      name
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onDeleteContext = /* GraphQL */ `
  subscription OnDeleteContext($owner: String!) {
    onDeleteContext(owner: $owner) {
      id
      name
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onCreateTodoTag = /* GraphQL */ `
  subscription OnCreateTodoTag($owner: String!) {
    onCreateTodoTag(owner: $owner) {
      id
      todoId
      tagId
      tag {
        id
        name
        color
        todos {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todo {
        id
        tags {
          nextToken
          startedAt
        }
        todoProjectId
        project {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todoContextId
        context {
          id
          name
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        title
        description
        state
        waitingFor
        scheduledOn
        doneOn
        owner
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateTodoTag = /* GraphQL */ `
  subscription OnUpdateTodoTag($owner: String!) {
    onUpdateTodoTag(owner: $owner) {
      id
      todoId
      tagId
      tag {
        id
        name
        color
        todos {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todo {
        id
        tags {
          nextToken
          startedAt
        }
        todoProjectId
        project {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todoContextId
        context {
          id
          name
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        title
        description
        state
        waitingFor
        scheduledOn
        doneOn
        owner
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteTodoTag = /* GraphQL */ `
  subscription OnDeleteTodoTag($owner: String!) {
    onDeleteTodoTag(owner: $owner) {
      id
      todoId
      tagId
      tag {
        id
        name
        color
        todos {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todo {
        id
        tags {
          nextToken
          startedAt
        }
        todoProjectId
        project {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todoContextId
        context {
          id
          name
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        title
        description
        state
        waitingFor
        scheduledOn
        doneOn
        owner
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
      owner
    }
  }
`;
