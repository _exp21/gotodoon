/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const syncTodos = /* GraphQL */ `
  query SyncTodos(
    $filter: ModelTodoFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncTodos(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        tags {
          nextToken
          startedAt
        }
        todoProjectId
        project {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todoContextId
        context {
          id
          name
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        title
        description
        state
        waitingFor
        scheduledOn
        doneOn
        owner
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
      }
      nextToken
      startedAt
    }
  }
`;
export const getTodo = /* GraphQL */ `
  query GetTodo($id: ID!) {
    getTodo(id: $id) {
      id
      tags {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      todoProjectId
      project {
        id
        name
        color
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      todoContextId
      context {
        id
        name
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      title
      description
      state
      waitingFor
      scheduledOn
      doneOn
      owner
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
    }
  }
`;
export const listTodos = /* GraphQL */ `
  query ListTodos(
    $filter: ModelTodoFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTodos(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        tags {
          nextToken
          startedAt
        }
        todoProjectId
        project {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todoContextId
        context {
          id
          name
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        title
        description
        state
        waitingFor
        scheduledOn
        doneOn
        owner
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
      }
      nextToken
      startedAt
    }
  }
`;
export const syncTags = /* GraphQL */ `
  query SyncTags(
    $filter: ModelTagFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncTags(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        name
        color
        todos {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      nextToken
      startedAt
    }
  }
`;
export const getTag = /* GraphQL */ `
  query GetTag($id: ID!) {
    getTag(id: $id) {
      id
      name
      color
      todos {
        items {
          id
          todoId
          tagId
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
          owner
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const listTags = /* GraphQL */ `
  query ListTags(
    $filter: ModelTagFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTags(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        color
        todos {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      nextToken
      startedAt
    }
  }
`;
export const syncProjects = /* GraphQL */ `
  query SyncProjects(
    $filter: ModelProjectFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncProjects(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        name
        color
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      nextToken
      startedAt
    }
  }
`;
export const getProject = /* GraphQL */ `
  query GetProject($id: ID!) {
    getProject(id: $id) {
      id
      name
      color
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const listProjects = /* GraphQL */ `
  query ListProjects(
    $filter: ModelProjectFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listProjects(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        color
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      nextToken
      startedAt
    }
  }
`;
export const syncContexts = /* GraphQL */ `
  query SyncContexts(
    $filter: ModelContextFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncContexts(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        name
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      nextToken
      startedAt
    }
  }
`;
export const getContext = /* GraphQL */ `
  query GetContext($id: ID!) {
    getContext(id: $id) {
      id
      name
      _version
      _deleted
      _lastChangedAt
      createdOn
      updatedOn
      owner
    }
  }
`;
export const listContexts = /* GraphQL */ `
  query ListContexts(
    $filter: ModelContextFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listContexts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
        owner
      }
      nextToken
      startedAt
    }
  }
`;
export const syncTodoTags = /* GraphQL */ `
  query SyncTodoTags(
    $filter: ModelTodoTagFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncTodoTags(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        todoId
        tagId
        tag {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todo {
          id
          todoProjectId
          todoContextId
          title
          description
          state
          waitingFor
          scheduledOn
          doneOn
          owner
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
        owner
      }
      nextToken
      startedAt
    }
  }
`;
export const todosByState = /* GraphQL */ `
  query TodosByState(
    $state: StateName
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTodoFilterInput
    $limit: Int
    $nextToken: String
  ) {
    todosByState(
      state: $state
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        tags {
          nextToken
          startedAt
        }
        todoProjectId
        project {
          id
          name
          color
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        todoContextId
        context {
          id
          name
          _version
          _deleted
          _lastChangedAt
          createdOn
          updatedOn
          owner
        }
        title
        description
        state
        waitingFor
        scheduledOn
        doneOn
        owner
        _version
        _deleted
        _lastChangedAt
        createdOn
        updatedOn
      }
      nextToken
      startedAt
    }
  }
`;
